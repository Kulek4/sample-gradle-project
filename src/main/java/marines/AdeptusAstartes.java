package marines;

public class AdeptusAstartes {

    private String name;
    private boolean isMoving;
    private boolean isFighting;

    public AdeptusAstartes(String name) {
        this.name = name;
        this.isMoving = false;
        this.isFighting = false;
    }

    public String getName() {
        return name;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public boolean isFighting() {
        return isFighting;
    }

    public void move() {
        this.isMoving = true;
    }

    public void stop() {
        this.isMoving = false;
    }

    public void fight() {
        this.isFighting = true;
    }

    public void disengage() {
        this.isFighting = false;
    }
}
