package marines;

import java.util.ArrayList;
import java.util.List;

public class Squad {

    private List<AdeptusAstartes> members;

    public Squad() {
        this.members = new ArrayList<>();
    }

    public List<AdeptusAstartes> getMembers() {
        return members;
    }

    public void addMember(AdeptusAstartes astartes) {
        members.add(astartes);
    }

    public void removeMember(AdeptusAstartes astartes) {
        members.remove(astartes);
    }

    public AdeptusAstartes getMember(AdeptusAstartes astartes) {
        return members.get(findGivenMemberIndex(astartes));
    }

    private int findGivenMemberIndex(AdeptusAstartes astartes) {
        int index = -1;
        for (AdeptusAstartes member : members) {
            if (member.getName().equalsIgnoreCase(astartes.getName())) {
                index = members.indexOf(member);
            }
        }
        return index;
    }


}
