package marine;

import marines.AdeptusAstartes;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestAdeptusAstartes {

    @Test
    public void shouldCheckInitialStatus() {
        // given & when
        String name = "Helbrecht";
        AdeptusAstartes astartes = new AdeptusAstartes(name);

        // then
        assertThat(astartes.getName()).isEqualTo(name);
        assertThat(astartes.isMoving()).isFalse();
        assertThat(astartes.isFighting()).isFalse();
    }

    @Test
    public void shouldMove() {
        // given & when
        String name = "Uriel Ventris";
        AdeptusAstartes astartes = new AdeptusAstartes(name);
        astartes.move();

        // then
        assertThat(astartes.isMoving()).isTrue();
    }

    @Test
    public void shouldFight() {
        // given & when
        String name = "Gabriel Angelos";
        AdeptusAstartes astartes = new AdeptusAstartes(name);
        astartes.fight();

        // then
        assertThat(astartes.isFighting()).isTrue();
    }

    @Test
    public void shouldDisengage() {
        // given & when
        String name = "Gabriel Angelos";
        AdeptusAstartes astartes = new AdeptusAstartes(name);
        astartes.fight();
        astartes.disengage();

        // then
        assertThat(astartes.isFighting()).isFalse();
    }

    @Test
    public void shouldStop() {
        // given & when
        String name = "Barabas Dantioch";
        AdeptusAstartes astartes = new AdeptusAstartes(name);
        astartes.move();
        astartes.stop();

        // then
        assertThat(astartes.isMoving()).isFalse();
    }

    @Test void shouldMoveAndFight() {
        // given & when
        String name = "Garviel Loken";
        AdeptusAstartes astartes = new AdeptusAstartes(name);
        astartes.move();
        astartes.fight();

        // then
        assertThat(astartes.isMoving()).isTrue();
        assertThat(astartes.isFighting()).isTrue();
    }
}
