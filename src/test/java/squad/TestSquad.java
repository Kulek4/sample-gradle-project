package squad;

import marines.AdeptusAstartes;
import marines.Squad;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestSquad {

    @Test
    public void shouldAddMember() {
        // given & when
        AdeptusAstartes astartes = new AdeptusAstartes("Horus Aximand");
        Squad squad = new Squad();

        squad.addMember(astartes);

        // then
        assertThat(squad.getMembers().size()).isEqualTo(1);
        assertThat(squad.getMember(astartes).getName()).isEqualTo(astartes.getName());
    }

    @Test
    public void shouldRemoveMember() {
        // given & when
        AdeptusAstartes astartes = new AdeptusAstartes("Ezekyle Abbadon");
        Squad squad = new Squad();
        squad.addMember(astartes);
        squad.removeMember(astartes);

        // then
        assertThat(squad.getMembers().size()).isEqualTo(0);
    }
}
